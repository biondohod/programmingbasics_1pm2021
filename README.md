# Основы программирования 1ПМ осень 2021

Лекция 1  
https://www.youtube.com/watch?v=6z-D8akDzD4

Лекция 2  
https://www.youtube.com/watch?v=U9On5Dn8JTE

Лекция 3 Циклы см в тимсе https://teams.microsoft.com/l/team/19%3abeN2iz4TmrcJYwGj0DL3RsSEGDCjBVctgg7mtX22a6A1%40thread.tacv2/conversations?groupId=26d049b9-e0bd-4e6e-a8fb-c49dc4f1bb6f&tenantId=6bf462b9-ace0-435b-94e5-2bb4d5189db7
https://youtu.be/F2WJIv5wdGs

Лекция 4 Массивы
https://youtu.be/Um7b2EDmfpc

Лекция 5 Строки  
https://youtu.be/qMU4gjkhdC4 https://youtu.be/C4bJmR6OovU   

Лекция 6 Функции  
https://www.youtube.com/watch?v=hm5otpCTX6g  

Дополнительные материалы по С++ (просто и понятно)  
https://ravesli.com/uroki-cpp/#toc-0

Дополнительные материалы для тех, кто хочет чуть-чуть вперед  
https://www.youtube.com/watch?v=2PM4TgCZIQs&list=PLys0IdlMg6XcwfGUf9Z5QUjKH9iM1T8PG

Скачать Visual Studio 2019 Community  
https://visualstudio.microsoft.com/ru/vs/

